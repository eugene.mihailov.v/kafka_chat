package ru.mihailov.kafka.chat;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.mihailov.kafka.chat.config.AppConfig;
import ru.mihailov.kafka.chat.controller.Bootstrap;

public class App {

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        final Bootstrap bootstrap = applicationContext.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
