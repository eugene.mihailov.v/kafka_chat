package ru.mihailov.kafka.chat.command.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Service;
import ru.mihailov.kafka.chat.command.AbstractCommand;
import ru.mihailov.kafka.chat.consumer.ConsumerListener;
import ru.mihailov.kafka.chat.controller.Bootstrap;

@Service
public class LoginCommand extends AbstractCommand {

    private static final String COMMAND = "login";

    @Autowired
    private ConsumerFactory<String, String> consumerFactory;

    public LoginCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Sign up the chat.";
    }

    @Override
    public void execute() {
        if (bootstrap.getLogin() == null) {
            System.out.println("[SIGN UP]");
            System.out.println("ENTER YOUR NAME:");
            final String name = bootstrap.nextLine();
            bootstrap.setLogin(name);
            new ConsumerListener(consumerFactory, name);
            System.out.println("[OK]");
        } else {
            System.out.println(String.format("You are logged as '%s' already.", bootstrap.getLogin()));
        }
        System.out.println();
    }

}
