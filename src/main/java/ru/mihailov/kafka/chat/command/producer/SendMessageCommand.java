package ru.mihailov.kafka.chat.command.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mihailov.kafka.chat.command.AbstractCommand;
import ru.mihailov.kafka.chat.config.AppConfig;
import ru.mihailov.kafka.chat.controller.Bootstrap;
import ru.mihailov.kafka.chat.producer.Sender;

@Service
public class SendMessageCommand extends AbstractCommand {

    private static final String COMMAND = "send";
    private static final String SEND_ALL = "ALL";

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private Sender sender;

    public SendMessageCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Send a message.";
    }

    @Override
    public void execute() {
        System.out.println("[SEND MESSAGE]");
        System.out.println("ENTER RECEIVER NAME (or enter ALL to send for all receivers):");
        final String name = bootstrap.nextLine();
        if (!name.equals(SEND_ALL) && bootstrap.getLogin() != null && bootstrap.getLogin().equals(name)) {
            System.out.println("You can not send the message yourself.");
        } else {
            System.out.println("ENTER MESSAGE:");
            final String message = bootstrap.nextLine();
            sender.send(name.equals(SEND_ALL) ? appConfig.getCommonTopic() : name, message);
            System.out.println("[OK]");
        }
        System.out.println();
    }

}
