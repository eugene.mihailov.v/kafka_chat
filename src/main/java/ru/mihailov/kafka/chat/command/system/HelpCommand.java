package ru.mihailov.kafka.chat.command.system;

import org.springframework.stereotype.Service;
import ru.mihailov.kafka.chat.command.AbstractCommand;
import ru.mihailov.kafka.chat.controller.Bootstrap;

@Service
public final class HelpCommand extends AbstractCommand {

    private static final String COMMAND = "help";

    public HelpCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (AbstractCommand command: bootstrap.getListCommand()) {
            System.out.println(command.command()+ ": " + command.description());
        }
    }

}
