package ru.mihailov.kafka.chat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.mihailov.kafka.chat.*")
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Value("${common.topic}")
    private String commonTopic;

    @Value("${broker.address}")
    private String brokerAddress;

    public String getCommonTopic() {
        return commonTopic;
    }

    public void setCommonTopic(String commonTopic) {
        this.commonTopic = commonTopic;
    }

    public String getBrokerAddress() {
        return brokerAddress;
    }

    public void setBrokerAddress(String brokerAddress) {
        this.brokerAddress = brokerAddress;
    }

}
