package ru.mihailov.kafka.chat.consumer;

import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.MessageListener;

public class ConsumerListener {

    public ConsumerListener(ConsumerFactory<String, String> consumerFactory, String topic) {
        MessageListener<String, String> messageListener = record ->
                System.out.println(String.format("Received the message='%s' from the topic='%s'", record.value(), topic));

        ConcurrentMessageListenerContainer container =
                new ConcurrentMessageListenerContainer<>(
                        consumerFactory,
                        containerProperties(topic, messageListener));

        container.setConcurrency(3);
        container.getContainerProperties().setPollTimeout(3000);
        container.start();
    }

    private ContainerProperties containerProperties(String topic, MessageListener<String, String> messageListener) {
        ContainerProperties containerProperties = new ContainerProperties(topic);
        containerProperties.setMessageListener(messageListener);
        return containerProperties;
    }

}
