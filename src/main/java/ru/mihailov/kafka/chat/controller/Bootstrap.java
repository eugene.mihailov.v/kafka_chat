package ru.mihailov.kafka.chat.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Component;
import ru.mihailov.kafka.chat.command.AbstractCommand;
import ru.mihailov.kafka.chat.config.AppConfig;
import ru.mihailov.kafka.chat.consumer.ConsumerListener;
import ru.mihailov.kafka.chat.error.CommandCorruptException;

@Component
public class Bootstrap {

    @Autowired
    private AppConfig appConfig;
    @Autowired
    private ConsumerFactory<String, String> consumerFactory;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    private String login;

    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    public void start() throws Exception {
        startCommonListener();
        System.out.println("*** WELCOME TO KAFKA CHAT ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    private void startCommonListener() {
        new ConsumerListener(consumerFactory, appConfig.getCommonTopic());
    }

    public List<AbstractCommand> getListCommand() {
        return new ArrayList<>(commands.values());
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
